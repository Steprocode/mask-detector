import dearpygui.dearpygui as dpg
import cv2 as cv
import numpy as np
from threading import Thread
import threading
from time import sleep
import time
from PIL import Image
import os
import colorsys
from squeezeNetModel import ModelSqueezeNet

"""
The purpose of the program is to detect person with or without mask.
It uses DearPyGUI for handling graphical user interface and SqueezeNet 
neural network for mask and person recognition from a captured image
from camera. Program uses 2 separate threads for updating image in GUI
and the other for mask detection. Thanks to this solution user can see
smooth images that are captured from a camera as well as frequent updates
of NN.

Border around the image:
- green: person with mask detected
- red: person without mask detected
- black: no one in the image
"""


NO_PERSON_THRESHOLD = 0.80
TEXTURE_TAG = "texture_tag"
LABEL_TAG = "label_tag"
LABEL_NO_PERSON = "No person detected"
LABEL_MASK = "Person with mask detected"
LABEL_NO_MASK = "Person without mask detected"
WHITE = (255, 255, 255)
WINDOW_NAME = "Mask detector"
MODEL_NAME = "model_SqNet_NewData_BCELoss_3classes_final_v1.pth"
FPS = 60
EXTRA_WINDOW_SIZE = 70
CAM_NO = 0
NEURAL_NETWORK_SLEEP = 0.1


"""
Configure DearPyGUI library.
No return.
"""
def setup_dearpygui(frame, texture_data):
    dpg.create_context()
    with dpg.texture_registry(show=False):
        dpg.add_raw_texture(frame.shape[1], frame.shape[0], texture_data, tag=TEXTURE_TAG,
                            format=dpg.mvFormat_Float_rgb)

    with dpg.window(tag=WINDOW_NAME):
        dpg.add_image(TEXTURE_TAG)

    dpg.create_viewport(title=WINDOW_NAME, width=frame.shape[1] + EXTRA_WINDOW_SIZE, height=frame.shape[0] + EXTRA_WINDOW_SIZE)
    dpg.setup_dearpygui()
    dpg.show_viewport()
    dpg.set_primary_window(WINDOW_NAME, True)
    dpg.add_text("", tag=LABEL_TAG, color=WHITE, parent=WINDOW_NAME)


"""
Return cropped captured frame from a camera.
"""
def get_frame(vid):
    ret, frame = vid.read()
    size = min(frame.shape[0], frame.shape[1])
    x_pos = (frame.shape[1] - size) // 2
    y_pos = (frame.shape[0] - size) // 2

    frame = frame[y_pos: y_pos + size, x_pos: x_pos + size]
    return frame


"""
Return converted 2D frame to 1D normalized texture. 
"""
def convert_frame_to_texture(frame):
    data = np.flip(m=frame, axis=2)             # BGR to RGB
    data = data.ravel()                         # flatten camera data to a 1 d stricture
    data = np.asfarray(a=data, dtype='f')       # change data type to 32bit floats
    data = np.true_divide(data, 255.0)          # normalize image data to prepare for GPU
    return data


"""
Return color in tuple (R, G, B) based 
on values of probabilities.
"""
def determine_color(probabilities):
    hue_green = 90
    hue_red = 180
    saturation_multiplier = 255
    value_multiplier = 179

    hue_multiplier = abs(hue_red - hue_green) // 2
    hue_shift = min(hue_green, hue_red)
    hue = np.clip(hue_shift + hue_multiplier + (probabilities["person_no_mask"] * hue_multiplier) - (
                probabilities["person_with_mask"] * hue_multiplier), hue_shift, hue_shift + hue_multiplier * 2)
    hue = np.round(hue)

    saturation = saturation_multiplier - probabilities["no_person"] * saturation_multiplier
    value = value_multiplier - probabilities["no_person"] * value_multiplier
    (r, g, b) = colorsys.hsv_to_rgb(hue / 255, saturation / 255, value / value_multiplier)
    result = (int(r * 255), int(g * 255), int(b * 255))
    return result


"""
Return 2D image with added color border.
"""
def add_color_border(image, color, border=5):
    height = image.shape[0] + 2 * border
    width = image.shape[1] + 2 * border

    result = np.zeros([height, width, 3])
    result[:, :] = np.array(color, dtype=np.ubyte)
    result[border: image.shape[0] + border, border: image.shape[1] + border] = image
    return result


"""
Thread responsible for capturing frame 
and drawing it in window.
"""
def update_frame_thread(vid, fps, stop_threads, lock):
    global border_color, period
    global frame
    while True:
        if stop_threads():
            return
        if dpg.is_dearpygui_running() == False:
            continue

        start_time = time.time()

        lock.acquire()
        frame = get_frame(vid)
        bordered = add_color_border(frame, border_color)
        lock.release()

        texture_data = convert_frame_to_texture(bordered)
        dpg.set_value(TEXTURE_TAG, texture_data)
        sleep(1 / fps)

        period = time.time() - start_time


"""
Clear console using system-based command.
"""
def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')


"""
Clear console and print formatted probabilities.
"""
def print_probabilities(probabilities):
    global period
    clear_console()
    print('  | {:20s} | {:20s} | {:20s} |'.format('person without mask', 'person with mask', 'no person'))
    print('-' * 72)
    print('% | {:20.2f} | {:20.2f} | {:20.2f} |'.format(probabilities["person_no_mask"] * 100,
                                                        probabilities["person_with_mask"] * 100,
                                                        probabilities["no_person"] * 100))
    print('')
    print('render fps: {}'.format(1 / period))


"""
Update GUI label based on provided probabilities.
"""
def update_label(probabilities):
    if max(probabilities["person_with_mask"], probabilities["person_no_mask"]) < NO_PERSON_THRESHOLD \
        or probabilities["no_person"] > max(probabilities["person_with_mask"], probabilities["person_no_mask"]):
        dpg.set_value(LABEL_TAG, LABEL_NO_PERSON)
    elif probabilities["person_with_mask"] > probabilities["person_no_mask"]:
        dpg.set_value(LABEL_TAG, LABEL_MASK)
    else:
        dpg.set_value(LABEL_TAG, LABEL_NO_MASK)


"""
Thread responsible for mask detection and determining
color of the border.
"""
def detect_mask_thread(stop_threads, lock, model):
    global frame, probabilities, border_color
    while True:
        if stop_threads():
            return
        lock.acquire()
        image = frame.copy()
        lock.release()

        PIL_image = Image.fromarray(image)
        result = model.get_pred(PIL_image)

        lock.acquire()
        probabilities["person_with_mask"] = result[1]
        probabilities["person_no_mask"] = result[2]

        probabilities["no_person"] = (max(result[1], result[2]) < NO_PERSON_THRESHOLD or result[0] > max(result[1], result[2])) * 1
        border_color = determine_color(probabilities)
        lock.release()
        print_probabilities(probabilities)
        update_label(probabilities)

        sleep(NEURAL_NETWORK_SLEEP)


"""
Main function responsible for initial setup 
and starting threads.
"""
def main():
    global probabilities, border_color, period
    period = 1
    border_color = (0, 0, 0)
    probabilities = {
        "person_no_mask": 0.0,
        "person_with_mask": 0.0,
        "no_person": 0.0
    }
    stop_threads = False
    lock = threading.Lock()

    model = ModelSqueezeNet(model_dir=MODEL_NAME, model_classes_num=3)

    vid = cv.VideoCapture(CAM_NO)
    frame = get_frame(vid)
    frame = add_color_border(frame, border_color)
    texture_data = convert_frame_to_texture(frame)

    setup_dearpygui(frame, texture_data)
    update_frame_thread_var = Thread(target=update_frame_thread, args=(vid, FPS, lambda: stop_threads, lock,))
    detect_mask_var = Thread(target=detect_mask_thread, args=(lambda: stop_threads, lock, model,))
    update_frame_thread_var.start()
    detect_mask_var.start()
    dpg.start_dearpygui()

    stop_threads = True
    update_frame_thread_var.join()
    detect_mask_var.join()
    vid.release()
    dpg.destroy_context()


if __name__ == "__main__":
    main()
