import cv2
import os
import random

"""
Script for splitting dataset into train and val sets 
"""


def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder, filename))
        if img is not None:
            images.append(img)
    return images


def save_images_to_folder(folder, images):
    for iterator, image in enumerate(images):
        i = os.path.join(folder, (str(iterator) + '.jpg'))
        cv2.imwrite(i, image)


def split_and_save(images, path, species):
    save_path = os.path.join(path, species)
    if not os.path.exists(save_path):
        os.mkdir(save_path)
    save_images_to_folder(save_path, images)


def prepare_data():
    split_train = 0.8
    split_val = 0.2
    raw_data_path = "C:/ImageProcessing_project/dataset/FaceMask-Dataset-covid-19"
    train_path = "C:/ImageProcessing_project/dataset/data_3/train"
    val_path = "C:/ImageProcessing_project/dataset/data_3/val"

    for types in os.listdir(raw_data_path):
        images = load_images_from_folder(f"{raw_data_path}/{types}")
        random.shuffle(images)
        split_im_num = int(len(images) * split_train)
        split_and_save(images[:split_im_num], train_path, types)
        split_and_save(images[split_im_num:], val_path, types)
        print(f'{types} splinted')


if __name__ == "__main__":
    prepare_data()
