import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms


"""
Neural network model architecture ModelSqueezeNet for SqueezeNet with 3 classes
Used for detecting if on image there is person with mask or without, or there is no person.
(class 0 probability for no person, 1 person with mask, 2 person without mask)
Provide image in PIL.Images format.
"""


class ModelSqueezeNet:
    def __init__(self, model_dir, model_classes_num):
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = torchvision.models.squeezenet1_1(weights=None)
        num_features = self.model.classifier[1].in_channels
        features = list(self.model.classifier.children())[:-3]
        features.extend([nn.Conv2d(num_features, model_classes_num, kernel_size=1)])
        features.extend([nn.ReLU(inplace=True)])
        features.extend([nn.AdaptiveAvgPool2d(output_size=(1, 1))])
        self.model.classifier = nn.Sequential(*features)
        self.model.load_state_dict(torch.load(model_dir, map_location=torch.device(self.device)))
        self.model = self.model.to(self.device)

        self.transform = transforms.Compose([
            transforms.Resize(size=(224, 224)),
            transforms.ToTensor(),
            transforms.Grayscale(num_output_channels=3),
        ])

    def get_pred(self, image):  # forward pass
        image = self.transform(image)
        image = torch.unsqueeze(image, dim=0).to(self.device)
        with torch.inference_mode():
            prediction = self.model(image)
        return torch.softmax(prediction.squeeze(), dim=0).tolist()
        # return prediction.squeeze().tolist()
