import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import copy
import os
from torchvision.models import SqueezeNet1_1_Weights


"""One training step"""
def train_step(model, dataloader, loss_fun, optim, device, report_frequency):
    model.train()
    train_loss, train_acc = 0, 0
    for batch, (images, labels) in enumerate(dataloader):
        images = images.to(device)
        labels = labels.to(device)
        optim.zero_grad()
        prediction = model(images)
        loss = loss_fun(prediction, labels)  # CrossEntropy Loss

        loss.backward()
        optim.step()
        train_loss += loss.item()
        predicted_classes = torch.argmax(prediction, 1)
        train_acc += (torch.sum(predicted_classes == labels.data).item() / len(prediction))
        if batch % report_frequency == 0 and batch > 1:
            print(f"batch {batch}/{len(dataloader)} train: acc {(train_acc / batch):.4f} loss {(train_loss / batch):.4f}")
    return (train_acc / len(dataloader)), (train_loss / len(dataloader))


"""One validation step"""
def val_step(model, dataloader, loss_fun, device, report_frequency):
    model.eval()
    val_loss, val_acc = 0, 0
    with torch.no_grad():
        for batch, (images, labels) in enumerate(dataloader):
            images = images.to(device)
            labels = labels.to(device)
            prediction = model(images)
            loss = loss_fun(prediction, labels)  # CrossEntropy Loss

            val_loss += loss.item()
            predicted_classes = torch.argmax(prediction, 1)
            val_acc += (torch.sum(predicted_classes == labels.data).item() / len(prediction))
            if batch % report_frequency == 0 and batch > 1:
                print(f"batch {batch}/{len(dataloader)} train: acc {(val_acc / batch):.4f} loss {(val_loss / batch):.4f}")
    return val_acc / len(dataloader), val_loss / len(dataloader)


"""
function for training the model, it prints the progress, and saving the best acc model 
"""
def train_model(model, train_loader, val_loader, loss, optim, epochs, device, report_frequency):
    best_model_weights = copy.deepcopy(model.state_dict())
    best_acc = 0.0
    for epoch in range(epochs):
        print(f"Training epoch : {epoch + 1}")
        train_acc, train_loss = train_step(model, train_loader, loss, optim, device, report_frequency)
        print(f"Validation epoch : {epoch + 1}")
        val_acc, val_loss = val_step(model, val_loader, loss, device, report_frequency)
        print(f"Epoch {epoch + 1}/{epochs}, train: acc {train_acc:.4f} loss {train_loss:.4f} |"
              f" val: acc {val_acc:.4f} loss {val_loss:.4f}")
        if val_acc > best_acc:
            best_acc = val_acc
            best_model_weights = copy.deepcopy(model.state_dict())
            print(f"Found new best acc : {best_acc}")
    print("Training finished")
    model.load_state_dict(best_model_weights)
    return model


"""
Main function for training the model, there are all hyper parameters needed for training and all training elements
"""
def main():
    # hyper parameters
    num_epochs = 5
    batch_size = 12

    learning_rate = 0.001
    report_frequency = 1000

    train_dir = "C:/ImageProcessing_project/dataset/data_3/train"
    val_dir = "C:/ImageProcessing_project/dataset/data_3/val"
    model_save_path = "model_SqNet_NewData_BCELoss_3classes_final_v1.pth"
    im_size = (224, 224)
    train_device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # preparing transforms
    transform_train = transforms.Compose(
        [
            transforms.Resize(size=im_size),
            transforms.ToTensor(),
            transforms.Grayscale(num_output_channels=3),
            transforms.RandomHorizontalFlip(),
        ])
    transform_val = transforms.Compose(
        [
            transforms.Resize(size=im_size),
            transforms.ToTensor(),
            transforms.Grayscale(num_output_channels=3),
        ])

    # preparing datasets and data loaders
    train_dataset = torchvision.datasets.ImageFolder(os.path.join(train_dir), transform_train)
    val_dataset = torchvision.datasets.ImageFolder(os.path.join(val_dir), transform_val)
    train_dataloader = torch.utils.data.DataLoader(
        dataset=train_dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=0,
        pin_memory=True
    )
    val_dataloader = torch.utils.data.DataLoader(
        val_dataset,
        batch_size,
        shuffle=True,
        num_workers=0,
        pin_memory=True
    )
    data_classes = train_dataset.classes

    # model loading
    model_sq_net = torchvision.models.squeezenet1_1(weights=SqueezeNet1_1_Weights.DEFAULT)
    # modifying model classifier layer, such that it outputs 3 classes
    num_features = model_sq_net.classifier[1].in_channels
    features = list(model_sq_net.classifier.children())[:-3]
    features.extend([nn.Conv2d(num_features, len(data_classes), kernel_size=1)])
    features.extend([nn.ReLU(inplace=True)])
    features.extend([nn.AdaptiveAvgPool2d(output_size=(1, 1))])
    # freezing model weights
    for param in model_sq_net.parameters():
        param.require_grad = False
    model_sq_net.classifier = nn.Sequential(*features)
    model_sq_net.to(train_device)
    model = model_sq_net

    # setting criterion and optimizer
    criterion = nn.CrossEntropyLoss().to(train_device)
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate, momentum=0.9)

    # running the training
    new_best = train_model(model, train_dataloader, val_dataloader, criterion, optimizer, num_epochs, train_device, report_frequency)

    # saving best model
    torch.save(new_best.state_dict(), model_save_path)


if __name__ == "__main__":
    main()
